﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
public struct TileCord
{
	public int row, column;

	public TileCord(Vector3 position) 
	{
		row = Mathf.FloorToInt (position.z);
		column = Mathf.FloorToInt (position.x);
	}
	public TileCord(int col, int ro)
	{
		row = ro;
		column = col;
	}
	public bool IsEqual(TileCord t1)
	{
		return t1.row == row && t1.column == column;
	}
	public Vector3 TileCenter
	{
		get{return new Vector3(column + 0.5f, 0, row + 0.5f);}
	}
	public TileCord LeftNeighbour
	{
		get{return new TileCord(column - 1, row);}
	}
	public TileCord RightNeighbour
	{
		get{return new TileCord(column + 1, row);}
	}
	public TileCord UpNeighbour
	{
		get{return new TileCord(column, row + 1);}
	}
	public TileCord DownNeighbour
	{
		get{return new TileCord(column, row - 1);}
	}
};
public class Tile : MonoBehaviour{
	public float[] smells;
	public bool visited;
	public GameObject obj;
	public int size;
	public bool isWall;
	public List<GameObject> content;
	
	void Awake()
	{
		content = new List<GameObject>();
		smells = new float[10];
	}

	public void Reset()
	{
		visited = false;
		for(int i = 0; i < smells.Length; i++) 
		{
			smells[i] = 0;
		}
		content.Clear();
	}
	public TileCord Cord
	{
		get{return new TileCord(obj.transform.localPosition);}
	}
	public void PropagateSmell(Tile previous)
	{

		for(int i = 0; i < smells.Length; i++)
		{
			if(previous.smells[i] == 0) continue;
			smells[i] = Mathf.Max(smells[i], previous.smells[i] - 1);
		}
	}

}
