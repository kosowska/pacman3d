﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Ghost : MonoBehaviour {
	public float[] smellweights;
	Moving moving;
	private float timer;
	private Color color;
	public SmellId smellId;
	public static readonly SmellId[] GhostSmells = new SmellId[]{SmellId.Ghost1, SmellId.Ghost2, SmellId.Ghost3};

	[Serializable]
	public class MeshForState
	{
		public State state;
		public GameObject view;
		public float speed = 1;
	}
	
	public List<MeshForState> states = new List<MeshForState>();

	public enum State
	{
		Normal,
		Scared,
		Dead
	}

	public State currentState = State.Normal;

	public bool debug;
	public Tile Tile {
		get
		{
			return MainGame.GetTile(new TileCord(transform.localPosition));
		}
	}
	public bool isEdible {
		get { return smellweights[1] != 0; }
	}

	public void ChangeMesh()
	{
		foreach (var meshForState in states)
		{
			meshForState.view.SetActive(meshForState.state == currentState);
		}
	}

	public void UpdateGhostSmells()
	{
		switch (currentState)
		{
			case State.Normal:
				foreach (var smell in GhostSmells)
				{
					if (smell == smellId) continue;
					smellweights[(int) smell] = -100;
				}
				break;
			case State.Dead:
			case State.Scared:
				foreach (var smell in GhostSmells)
				{
					smellweights[(int) smell] = 0;
				}
				break;
		}
		
	}
	void GhostEffects()
	{
		foreach (var meshForState in states)
		{
			//meshForState.view.SetActive(meshForState.state == currentState);
			if (meshForState.state == State.Dead)
			{
				color.r = ((int)color.r) ^ 1;
				color.g = ((int)color.g) ^ 1;
				color.b = ((int)color.b) ^ 1;
				timer = Time.time;
				meshForState.view.transform.GetComponent<MeshRenderer>().material.color = color;
			}
		}
	}
	void Start ()
	{
		moving = GetComponent<Moving>();
		color = new Color(1,1,1);
	}
	void FixedUpdate()
	{
		UpdateGhostSmells();
		ChangeMesh();
		GetComponent<Moving>().speed = states.First(s => s.state == currentState).speed;
		if(Time.time-timer >= 0.25f)GhostEffects();
		TileCord[] neighbours = new TileCord[4];
		neighbours[0] = Tile.Cord.LeftNeighbour;
		neighbours[1] = Tile.Cord.RightNeighbour;
		neighbours[2] = Tile.Cord.UpNeighbour;
		neighbours[3] = Tile.Cord.DownNeighbour;
		float maxWght = float.NegativeInfinity; 
		TileCord bestTileCord = new TileCord(0,0);
		foreach(var tc in neighbours)
		{
			float sum = 0;
			var t = MainGame.GetTile(tc);
			if (t == null) {
				continue;
			}
			if(MainGame.IsTileWalkable(t.Cord) == false) continue;
			for(int  i = 0; i< smellweights.Length; i++)
			{
				sum += t.smells[i] * smellweights[i];
			}
			if(sum > maxWght) {
				maxWght = sum;
				bestTileCord = tc;

				
			}
		}
		moving.nextDirection = (bestTileCord.TileCenter - Tile.Cord.TileCenter).normalized;
	}
}
