﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour {
	MainGame mainGame;
	public int xMax, xMin;
	void Start () {
		mainGame = MainGame.instance;
	}
	
	// Update is called once per frame
	void Update () {
		if(transform.position.x > mainGame.xMax){
		//if(transform.position.x > xMax){
			transform.position = new Vector3(mainGame.xMin,transform.position.y,transform.position.z);
		}
		if(transform.position.x < mainGame.xMin){
		//if(transform.position.x < xMin){
			transform.position = new Vector3(mainGame.xMax,transform.position.y,transform.position.z);
		}
	}
}
