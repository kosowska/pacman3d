﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Steering : MonoBehaviour {
	Moving moving;
	Pacman pacman;
	void CheckDirection()
	{
		if(Input.GetKey(KeyCode.UpArrow)) {
			MainGame.instance.isPacmanAlive = true;
			moving.nextDirection = new Vector3(0,0,1);
		}
		else if(Input.GetKey(KeyCode.DownArrow)) {
			MainGame.instance.isPacmanAlive = true;
			moving.nextDirection = new Vector3(0,0,-1);
		}
		else if(Input.GetKey(KeyCode.RightArrow)) {
			MainGame.instance.isPacmanAlive = true;
			moving.nextDirection = new Vector3(1,0,0);
		}
		else if(Input.GetKey(KeyCode.LeftArrow)) {
			MainGame.instance.isPacmanAlive = true;
			moving.nextDirection = new Vector3(-1,0,0);
		}
	}
	// Use this for initialization
	void Start () {
		moving = GetComponent<Moving>();
		pacman = GetComponent<Pacman>();
	}
	
	// Update is called once per frame
	void Update () {
//		Debug.Log(pacman.lives);
		if(pacman.lives > 0) CheckDirection();
	}
}
