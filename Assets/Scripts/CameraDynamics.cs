﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDynamics : MonoBehaviour
{
    public float minZ;

    public float maxZ;

    public Transform minTransform;

    public Transform maxTransform;
    private GameObject pacman;
    
    // Start is called before the first frame update
    void Start()
    {
        pacman = MainGame.instance.pacman;
    }

    // Update is called once per frame
    void Update()
    {
        var p = Mathf.Clamp01((pacman.transform.position.z - minZ) / (maxZ - minZ));
        Debug.Log(pacman.transform.position.ToString());
        transform.position = Vector3.Lerp(minTransform.position, maxTransform.position, p);
        transform.rotation = Quaternion.Slerp(minTransform.rotation, maxTransform.rotation, p);
    }
}
