﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleCollision : MonoBehaviour {
	public bool IsColliding(GameObject obj)
	{
		var c0 = GetComponent<CircleCollision>();
		var c1 = obj.GetComponent<CircleCollision>();
		return (Vector3.Distance(obj.transform.localPosition, transform.localPosition) < c0.radius + c1.radius);
	}
	public float radius = 1f;
}
