﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SmellId : int
{
	PacmanIsFood,
	GhostIsFood,
	GoToHome,
	Ghost1,
	Ghost2,
	Ghost3
	
}
public class SmellSource : MonoBehaviour {
	public List<float> smells;
	public Tile Tile {
		get
		{
			return MainGame.GetTile(new TileCord(transform.localPosition));
		}
	}
	// Use this for initialization
	public void InitSmells()
	{
		for(int i = 0; i < smells.Count; i ++)
		{
			Tile.smells[i] += smells[i];
		}
	}
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
