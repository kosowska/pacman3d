﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class Pacman : MonoBehaviour 
{
	public Vector3 startPosition;
	MainGame game;
	public AudioClip deathSound;
	public AudioClip moveSound;
	public AudioClip winSound;
	public AudioClip gameoverSound;
	public AudioSource source;
	float startTime;
	//public GameObject startPosition;
	public GameObject winText;
	public GameObject gameoverText;
	public GameObject startPosText;
	public GameObject endPosText;
	float animationStartTime;
	Vector3 goPos;
	float currentVal;
	int coinNumber;
	public GameObject coins;
	public bool alive;
	public float step;
	float zStep, xStep;
	public int score;
	bool gameover;
	public int lives;
	List<GameObject> toRemove;
	

	static public float BounceEaseOut(float p)
	{
		if(p < 4/11.0f)
		{
			return (121 * p * p)/16.0f;
		}
		else if(p < 8/11.0f)
		{
			return (363/40.0f * p * p) - (99/10.0f * p) + 17/5.0f;
		}
		else if(p < 9/10.0f)
		{
			return (4356/361.0f * p * p) - (35442/1805.0f * p) + 16061/1805.0f;
		}
		else
		{
			return (54/5.0f * p * p) - (513/25.0f * p) + 268/25.0f;
		}
	}
	void AnimateText(GameObject textToAnimate)
	{
		if(currentVal < 1f)
		{
			currentVal = BounceEaseOut(Time.time - animationStartTime);
			goPos = textToAnimate.transform.position;
			textToAnimate.transform.position = new Vector3(goPos.x, 
										startPosText.transform.position.y - currentVal*(startPosText.transform.position.y-endPosText.transform.position.y), 
										goPos.z);
		}
	}
	bool isGameOver()
	{
		if(lives <= 0)
		{
			return true;
		}
		return false;
	}
	bool isWin()
	{
		if(FindObjectsOfType<Coin>().Length == 0) {
			MainGame.instance.isPacmanAlive = false;
			MainGame.instance.bloomLayer.intensity.value = 5f;
			
			return true;

		}
		return false;
	}
	void Awake ()
	{
		source = GetComponent<AudioSource>();
	}
	void KillGhost(GameObject ghost)
	{
		if (ghost.GetComponent<Ghost>().smellweights[2] == 0)
		{
			
			score += 5;
			ghost.GetComponent<Ghost>().smellweights[2] = 5;
			ghost.GetComponent<Ghost>().currentState = Ghost.State.Dead;
		}

	}
	void Die()
	{
		for(int i = 0; i < MainGame.instance.ghosts.Count; i++)
		{
			MainGame.instance.ghosts[i].transform.position = MainGame.instance.ghostPositions[i];
			MainGame.instance.ghosts[i].GetComponent<Ghost>().currentState = Ghost.State.Normal;
		}
		MainGame.instance.hearts[3-lives].SetActive(false);
		if(MainGame.instance.isPlaying(deathSound,startTime) == false)
		{
			source.PlayOneShot(deathSound);
			startTime = Time.time;
		}
		MainGame.instance.isPacmanAlive = false;
		lives -= 1;
		transform.localPosition = startPosition;
		if(isGameOver()){
			animationStartTime = Time.time-0.01f;
			currentVal = 0.1f;
			source.PlayOneShot(gameoverSound);
			gameoverText.SetActive(true);
		}
	}
	void Start () {

		winText = GameObject.Find("WinText");
		gameoverText = GameObject.Find("GameOver");
		startPosText = GameObject.Find("start");
		endPosText = GameObject.Find("end");
		gameoverText.transform.position = startPosText.transform.position;
		//currentVal = startVal;
		lives = 3;
		toRemove = new List<GameObject>();
		game = MainGame.GetInstance();
		winText.SetActive(false);
		gameoverText.SetActive(false);
		
		alive = false;
		score = 0;
		Debug.Log("coin");

		Debug.Log(coinNumber);
	}
	
	// Update is called once per frame
	void Update () {
		if(animationStartTime!=0){

			if(isGameOver()){
				AnimateText(gameoverText);
				MainGame.instance.lensLayer.intensity.value-=1.5f;
			}
			else AnimateText(winText);
		}
		toRemove.Clear();
		if(MainGame.instance.isPacmanAlive == false)return;
		if(isGameOver() || isWin())return;
		foreach(var col in MainGame.instance.colliders)
		{
			if(GetComponent<CircleCollision>().IsColliding(col) == false) continue;
			if(col.GetComponent<Ghost>() != null)
			{
				if(GetComponent<EatGhosts>().isEnabled == true && col.GetComponent<Ghost>().isEdible)
				{
					KillGhost(col);
				}
				else Die();

			}
			if(col.GetComponent<Coin>() != null)
			{
				col.SetActive(false);
				score += 1;
				MainGame.instance.scoreText.GetComponent<TextMeshPro>().text = "Score:" + score.ToString();
				toRemove.Add(col);

				if(isWin()){
					source.PlayOneShot(winSound);
					winText.SetActive(true);
					animationStartTime = Time.time-0.01f;
					currentVal = 0.1f;
					MainGame.instance.isPacmanAlive = false;
				}
			}
			if(col.GetComponent<SuperCoin>() != null)
			{
				GetComponent<EatGhosts>().startTime = Time.time;
				for(int i = 0; i < MainGame.instance.ghosts.Count; i++)
				{
					MainGame.instance.ghosts[i].GetComponent<Ghost>().smellweights[1] = -2;
					MainGame.instance.ghosts[i].GetComponent<Ghost>().currentState = Ghost.State.Scared;
				}
				//GetComponent<SmellSource>().smells[1] = 40;
			}
		}
		foreach(var col in toRemove)
		{
			MainGame.instance.colliders.Remove(col);
		}
	}

}
