﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmellAbsorber : MonoBehaviour
{
	public List<bool> smellsMask;
	public Tile Tile {
		get
		{
			return MainGame.GetTile(new TileCord(transform.localPosition));
		}
	}
	public void AddSelfToTile()
	{
		Tile.content.Add(gameObject);
	}

	public void Absorb(Tile t)
	{
		for(int i = 0; i< t.smells.Length;i++)
		{
			if(i < smellsMask.Count && smellsMask[i]) t.smells[i] = 0;
		}
	}
}
