﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;
using UnityEngine.Rendering.PostProcessing;
 using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

public class MainGame : MonoBehaviour {
	
	public bool isPacmanAlive;
	public static MainGame instance = null;
	public Vector3 startPosition;
	public float xMax, xMin;
	public GameObject eastWall;
	public GameObject westWall;
	public GameObject scoreText;
	string[,] boardStrings;
	Tile[,] board;
	
	public GameObject levelOrigin;
	public int tileSize;
	public GameObject wall;
	public GameObject plane;
	public GameObject pacmanPrefab;
	public GameObject pacman;
	Tile til;
	public SmellSource[] smellSources;
	
	public List<GameObject> colliders;
	public Bloom  bloomLayer = null;
	public LensDistortion lensLayer;
	public ColorGrading colorLayer;
	public List<GameObject> hearts;
	public List<GameObject> ghosts;
	public List<Vector3> ghostPositions;
	public void TaskOnClick()
	{
	//Output this to console when Button1 or Button3 is clicked
		//vignetteLayer.intensity.value = 9f;
		
		//colorLayer.postExposure.value =  new FloatParameter { value = -5f };
		SceneManager.LoadScene("Level1");
	}
	public bool isPlaying(AudioClip clip, float startTime)
	{
		if((Time.time - startTime) >= clip.length)
			return false;
		return true;
	}
	public static bool IsTileCordValid(TileCord c)
	{
		return c.row < instance.board.GetLength(1) && c.row >= 0 && c.column < instance.board.GetLength(0) && c.column >= 0;
	}
	public static bool IsTileWalkable (TileCord c)
	{
		if(IsTileCordValid(c) == false) return false;
		return !instance.board[c.column,c.row].isWall;
	}
	public static Tile GetTile(TileCord c)
	{
		if(IsTileCordValid(c) == false) return null;
		return instance.board[c.column,c.row];
	}
	void Awake()
  {
		board = new Tile[10,14];
		MakeBoard();
		MakePacman();
		colliders = FindObjectsOfType<CircleCollision>().Select(obj => obj.gameObject).ToList();
		foreach(var c in colliders)
		{
			if(c.GetComponent<Pacman>()!=null)continue;
			c.transform.position = new TileCord(c.transform.position).TileCenter;//new Vector3(c.transform.position.x+0.5f,c.transform.position.y,c.transform.position.z+0.5f);
		}
		//Debug.Log(colliders.Count);
		smellSources = FindObjectsOfType<SmellSource>();
      if (instance != null && instance != this)
          Destroy(gameObject);    // Ensures that there aren't multiple Singletons
 
      instance = this;
  
  }
	void MakeBoard()
	{
		var tiles = FindObjectsOfType<Tile>();
		foreach(var t in tiles)
		{
			t.obj = t.gameObject;
			var tc = new TileCord(t.transform.position);
			t.transform.position = tc.TileCenter; 
			board[tc.column, tc.row] = t;
			
			
		}
	}
	void MakePacman()
	{
		pacman = Instantiate(pacmanPrefab);
		pacman.transform.SetParent(levelOrigin.transform);
		pacman.transform.localPosition = new Vector3(2.5f,0,2.5f);
		pacman.GetComponent<Pacman>().startPosition = pacman.transform.localPosition;
	}

	void NotVisited(Tile currentTile)
	{
		currentTile.visited = false;
		TileCord[] neighbours = new TileCord[4];
		neighbours[0] = currentTile.Cord.LeftNeighbour;
		neighbours[1] = currentTile.Cord.RightNeighbour;
		neighbours[2] = currentTile.Cord.UpNeighbour;
		neighbours[3] = currentTile.Cord.DownNeighbour;
		for(int i = 0; i < 4; i ++)
		{
			if(IsTileWalkable(neighbours[i]) && GetTile(neighbours[i]).visited){
				GetTile(neighbours[i]).visited = false;
				NotVisited(GetTile(neighbours[i]));
			}
		}

	}
	void BFS(Queue<Tile> tiles)
	{
	//	Queue<Tile> tiles = new Queue<Tile>();
		Tile currentTile;
		TileCord[] neighbours = new TileCord[4];
		while(tiles.Count > 0)
		{
			currentTile = tiles.Dequeue();

			neighbours[0] = currentTile.Cord.LeftNeighbour;
			neighbours[1] = currentTile.Cord.RightNeighbour;
			neighbours[2] = currentTile.Cord.UpNeighbour;
			neighbours[3] = currentTile.Cord.DownNeighbour;
			for(int i = 0; i < 4; i ++)
			{
				if(IsTileWalkable(neighbours[i]) && !GetTile(neighbours[i]).visited){
					GetTile(neighbours[i]).visited = true;
					GetTile(neighbours[i]).PropagateSmell(currentTile);
					tiles.Enqueue(GetTile(neighbours[i]));
				}
			}
		}
		
	}
	void SpreadSmells()
	{
		for(int i = 0 ; i < board.GetLength(0); i++)
		{
			for(int j = 0; j < board.GetLength(1); j++)
			{
				if (board[i,j] == null) {
					Debug.Log("null tile " + i + ", " + j);
				}
				board[i,j].Reset();
			}
		}

		var absorbers = FindObjectsOfType<SmellAbsorber>();
		foreach( var ab in absorbers)
		{
			ab.AddSelfToTile();
		}

		Queue<Tile> tiles = new Queue<Tile>();
		for(int i = 0; i < smellSources.Length; i++)
		{
			NotVisited(smellSources[i].Tile);
			tiles.Enqueue(smellSources[i].Tile);
			Debug.Log("Mamy" + smellSources[i].Tile);
			smellSources[i].InitSmells();
			smellSources[i].Tile.visited = true;
			BFS(tiles);
			//tiles.Dequeue();
			
		} 
	}
	public static MainGame GetInstance()
    {
        if(instance == null)
        {
            instance = new MainGame();
        }
       
        return instance;
    }
	void Start()
	{
		for(int i = 0; i < ghosts.Count; i++)
		{
			ghostPositions.Add(ghosts[i].transform.position);
		}
		PostProcessVolume volume = gameObject.GetComponent<PostProcessVolume>();
 		volume.profile.TryGetSettings(out bloomLayer);
		volume.profile.TryGetSettings(out lensLayer);
		volume.profile.TryGetSettings(out colorLayer);
		//exposureLayer.eyeAdaptation.value
		colorLayer.postExposure.value = -10;
		bloomLayer.intensity.value = 0f;
		lensLayer.intensity.value = 0;

	}
	void Update()
	{
		if(colorLayer.postExposure.value < 0){
			colorLayer.postExposure.value += 0.5f;
		}
	}
	void FixedUpdate()
	{
		SpreadSmells();
	}

}
