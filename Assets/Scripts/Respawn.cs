﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		foreach(var col in MainGame.instance.colliders)
		{
			if(GetComponent<CircleCollision>().IsColliding(col) == false) continue;
			if(col.GetComponent<Ghost>() != null)
			{
				col.GetComponent<Ghost>().smellweights[2] = 0;
				col.GetComponent<Ghost>().smellweights[1] = 0;
				col.GetComponent<Ghost>().currentState = Ghost.State.Normal;

			}
		}
	}
}
