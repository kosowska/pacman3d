﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EatGhosts : MonoBehaviour {

	// Use this for initialization
	public float startTime;
	public bool isEnabled
	{
		get{return (Time.time - startTime <= 10f);}
	}
	void PowerOff()
	{
		//if(Time.time - startTime < 5f || Time.time - startTime > 7f) return;
		if (!isEnabled)
		{
			for(int i = 0; i < MainGame.instance.ghosts.Count; i++)
			{
				MainGame.instance.ghosts[i].GetComponent<Ghost>().smellweights[1] = 0;
				if (MainGame.instance.ghosts[i].GetComponent<Ghost>().currentState == Ghost.State.Dead) continue;
				MainGame.instance.ghosts[i].GetComponent<Ghost>().currentState = Ghost.State.Normal;
			}
			
		}
	}
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		PowerOff();
	}
}
