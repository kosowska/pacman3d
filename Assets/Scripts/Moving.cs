﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moving : MonoBehaviour {

	public Vector3 direction;
	public float speed;
	public Vector3 nextDirection;
	Pacman pacman;
	float startTime;
	private float timer;
	void ChangeDirection()
	{
		if (Mathf.Abs(Vector3.Dot(direction,nextDirection)) > Mathf.Epsilon) {
			direction = nextDirection;
			return;
		}
		
		var desiredTileCord = new TileCord(transform.localPosition + nextDirection);
		if(!MainGame.IsTileWalkable(desiredTileCord)){
			return;
		}
		if(GetNewPosition() == transform.localPosition) {
			direction = nextDirection;
			return;
		}
		var nextTileCord = new TileCord(transform.localPosition + direction * (speed * Time.fixedDeltaTime + 0.5f) );
		var oldTileCord = new TileCord(transform.localPosition + direction * 0.5f);
		if(!nextTileCord.IsEqual(oldTileCord)) {
			direction = nextDirection;
			return;
		}
	}

	Vector3 GetNewPosition()
	{
		var nextTileCord = new TileCord(transform.localPosition + direction * (speed * Time.fixedDeltaTime + 0.5f) );
		if (MainGame.IsTileWalkable(nextTileCord)) {
			if(this.GetComponent<Pacman>() != null && MainGame.instance.isPlaying(pacman.moveSound, startTime)==false){
				//source.PlayOneShot(moveSound);
				pacman.source.PlayOneShot(pacman.moveSound);
				startTime = Time.time;
			}
			return transform.localPosition + direction * speed * Time.fixedDeltaTime;
		}
		return new TileCord(transform.localPosition).TileCenter;
	}
	void Awake()
	{
		pacman = GetComponent<Pacman>();
	//	source = GetComponent<AudioSource>();
	}
	void Start () {
		pacman = GetComponent<Pacman>();
		//source = GetComponent<AudioSource>();
	}
	
	void isCollision()
	{

	}
	void Update () {
		
	}
	void FixedUpdate()
	{
		if(MainGame.instance.isPacmanAlive == true){
			ChangeDirection();
			transform.localPosition = GetNewPosition();
			timer = Time.time;
		}
	}
}
